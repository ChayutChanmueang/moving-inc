using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDetector : MonoBehaviour
{
    public Script_PlayerInfoAbilityHandler SPIAH;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SPIAH.SetCanGrabObj(collision.gameObject);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        SPIAH.UnSetCanGrabObj();
    }
}
