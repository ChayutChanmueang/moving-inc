using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Script_DropZone : MonoBehaviour
{
    public int PreferAmountofStuff = 0;
    public int CurrentAmountofStuff = 0;
    public Script_HUDManager HUDManager = null;
    public Script_PlayerInfoAbilityHandler SPIAH;
    public Script_SoundManager SoundManager;

    private bool EndButtonIsSelected = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(CurrentAmountofStuff>=PreferAmountofStuff)
        {
            Debug.Log("We got all stuff !");
            HUDManager.EndGamePanel.SetActive(true);
            if (!EndButtonIsSelected)
            {
                HUDManager.EndGameDefaultButton.Select();
                EndButtonIsSelected = true;
            }
            Time.timeScale = 0;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("GrabableObject"))
        {
            CurrentAmountofStuff++;
            if (SPIAH.GrabbingObj)
            {
                SPIAH.PlayerWeight -= SPIAH.GrabbingObj.GetComponent<GrabableObject>().Weight;
            }
            Destroy(collision.gameObject);
            SPIAH.GrabbingObj = null;
            SoundManager.PlayDeliverItemSound();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        /*if (collision.gameObject.CompareTag("GrabableObject"))
        {
            CurrentAmountofStuff--;
        }*/
    }
}
