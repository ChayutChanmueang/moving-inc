using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Timer : MonoBehaviour
{
    public Script_HUDManager HUDManager;
    float StartTime;
    public float TimeLimit;
    float CurTime = 0;
    public int TimeLeft;
    float RealTimeLimit = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartTime = Time.timeSinceLevelLoad;
    }
    private void Awake()
    {
        CurTime = 0;
        RealTimeLimit = TimeLimit + Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(CurTime>=RealTimeLimit)
        {
            HUDManager.TimeoutPanel.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            CurTime = Time.time;
        }
        TimeLeft = (int)RealTimeLimit - (int)CurTime;
    }
}
