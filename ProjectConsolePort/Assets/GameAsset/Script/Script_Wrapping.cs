using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Wrapping : MonoBehaviour
{
    public Script_Wrapping EndPoint;
    Vector3 EP;
    bool AbleToWrap = false;
    public GameObject Player;
    bool CanWarp = true;
    int Cooldown = 0;
    int MaxCooldown = 120;
    // Start is called before the first frame update
    void Start()
    {
        EP = EndPoint.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(!CanWarp && Cooldown<MaxCooldown)
        {
            Cooldown++;
        }
        else if(Cooldown >= MaxCooldown)
        {
            Cooldown = 0;
            CanWarp = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        AbleToWrap = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        AbleToWrap = false;
    }
    public void Wrap()
    {
        if(CanWarp&&AbleToWrap)
        {
            Player.transform.position = EP;
            EndPoint.CanWarp = false;
            Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }
}
