using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Script_MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("Lives") <= 0)
        {
            PlayerPrefs.SetInt("Money", 0);
            PlayerPrefs.SetInt("Lives", 5);
            PlayerPrefs.Save();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GoToLobby()
    {
        SceneManager.LoadScene("Lobby");
    }
    public void ExitGame()
    {
        PlayerPrefs.SetInt("Money", 0);
        PlayerPrefs.SetInt("Lives", 5);
        PlayerPrefs.Save();
        Application.Quit();
    }
}
