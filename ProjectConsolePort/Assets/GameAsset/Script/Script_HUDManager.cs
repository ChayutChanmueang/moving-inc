using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Script_HUDManager : MonoBehaviour
{
    public GameObject Player = null;
    public int Reward = 1000;
    public AudioSource MusicPlayer;
    [Header("Game HUD")]
    public TMP_Text WeightIndicator = null;
    public TMP_Text LivesIndicator = null;
    public TMP_Text TimerText = null;
    public TMP_Text ItemLeftText = null;
    public Script_DropZone ItemLeft = null;
    public Script_Timer Timer = null;
    [Header("Timeout HUD")]
    public GameObject TimeoutPanel = null;
    public Button TimeoutDefaultButton = null;
    [Header("Game Over HUD")]
    public GameObject GameOverPanel = null;
    public Button GameOverDefaultButton = null;
    [Header("Dead HUD")]
    public GameObject DeadPanel = null;
    public TMP_Text LiveLeft = null;
    public SpriteRenderer PlayerSprite = null;
    public Button DeadDefaultButton = null;
    [Header("Pause HUD")]
    public GameObject PausePanel = null;
    public Button PauseDefaultButton = null;
    [Header("End Game HUD")]
    public GameObject EndGamePanel = null;
    public TMP_Text RewardIndicator = null;
    public Button EndGameDefaultButton = null;
    // Start is called before the first frame update
    void Start()
    {
        GameOverPanel.SetActive(false);
        PausePanel.SetActive(false);
        EndGamePanel.SetActive(false);
        DeadPanel.SetActive(false);
        TimeoutPanel.SetActive(false);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(Player != null)
        {
            WeightIndicator.text = "Current Weight : " + Player.GetComponent<Script_PlayerInfoAbilityHandler>().PlayerWeight.ToString() + " KG";
            LivesIndicator.text = "X " + Player.GetComponent<Health>().currentLives;
            LiveLeft.text = "X " + Player.GetComponent<Health>().currentLives;
            RewardIndicator.text = Reward.ToString();
            TimerText.text = "Time : " + Timer.TimeLeft + " Seconds";
            ItemLeftText.text = ItemLeft.PreferAmountofStuff - ItemLeft.CurrentAmountofStuff + " Item Left";
        }
        if(Player.GetComponent<Health>().currentHealth<=0 && Player.GetComponent<Health>().currentLives >= 0)
        {
            DeadPanel.SetActive(true);
            DeadDefaultButton.Select();
            MusicPlayer.Stop();
        }
        if(Player.GetComponent<Health>().currentLives<0)
        {
            GameOverPanel.SetActive(true);
            GameOverDefaultButton.Select();
            MusicPlayer.Stop();
        }
    }
    public void RestartMap()
    {
        PlayerSprite.enabled = true;
        Player.GetComponent<Health>().Respawn();
        DeadPanel.SetActive(false);
        Time.timeScale = 1;
    }
    public void ResumeGame()
    {
        PausePanel.SetActive(false);
        Time.timeScale = 1;
    }
    public void PauseGame()
    {
        PausePanel.SetActive(true);
        PauseDefaultButton.Select();
        Time.timeScale = 0;
    }
    public void GoToMainMenu()
    {
        Time.timeScale = 1;
        PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + Reward);
        PlayerPrefs.Save();
        SceneManager.LoadScene("MainMenu");
    }
    public void GoToLobby()
    {
        Time.timeScale = 1;
        PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + Reward);
        PlayerPrefs.Save();
        SceneManager.LoadScene("Lobby");
    }
    public void GoToMainMenuNoReward()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }
    public void GoToLobbyNoReward()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Lobby");
    }
    public void GoToMainMenuGameOver()
    {
        Time.timeScale = 1;
        PlayerPrefs.SetInt("Money", 0);
        PlayerPrefs.SetInt("Lives", 5);
        PlayerPrefs.Save();
        SceneManager.LoadScene("MainMenu");
    }
}
