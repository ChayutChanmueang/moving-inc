using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakablFloor : MonoBehaviour
{
    public float AcceptableWeight = 75.0f;
    public float BreakDelay = 3.0f;
    public GameObject ParentBlock = null;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player")
        {
            if(collision.gameObject.GetComponent<Script_PlayerInfoAbilityHandler>().PlayerWeight>AcceptableWeight)
            {
                Destroy(ParentBlock,BreakDelay);
            }
        }
    }
}
