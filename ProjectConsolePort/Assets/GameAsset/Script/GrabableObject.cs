using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabableObject : MonoBehaviour
{
    [Header("Item Infomation:")]
    public float Weight = 0;
    public bool IsGrab = false;
    public GameObject Player = null;
    public Vector3 GrabOffset = new Vector3(0, 0, 0);
    public bool Grabable = false;
    public bool IsATrap = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody2D ObjRigidbody = gameObject.GetComponent<Rigidbody2D>();
        if (IsGrab == true)
        {
            ObjRigidbody.constraints = RigidbodyConstraints2D.None;
            ObjRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
            gameObject.transform.position = Player.transform.position + GrabOffset;
            if(IsATrap == true)
            {
                IsATrap = false;
                Destroy(ObjRigidbody.GetComponent<Script_TimeTrap>());
            }
        }
        else
        {
            if(IsATrap == false)
            {
                ObjRigidbody.constraints = RigidbodyConstraints2D.None;
            }
            gameObject.GetComponent<Rigidbody2D>().simulated = true;
        }
    }
}
