using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Script_Lobby : MonoBehaviour
{
    public TMP_Text LivesIndicator = null;
    public TMP_Text MoneyIndicator = null;
    int Lives = 0;
    int Money = 0;
    public int LivesBuyRate = 100;
    public string[] Maps;
    public Script_SoundManager SoundManager = null;
    // Start is called before the first frame update
    void Start()
    {
        Lives = PlayerPrefs.GetInt("Lives");
        Money = PlayerPrefs.GetInt("Money");
        LivesIndicator.text = PlayerPrefs.GetInt("Lives").ToString();
        MoneyIndicator.text = PlayerPrefs.GetInt("Money").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        LivesIndicator.text = "X " + PlayerPrefs.GetInt("Lives");
        MoneyIndicator.text = PlayerPrefs.GetInt("Money").ToString();
    }
    public void GoToGame()
    {
        int Num = Random.Range(0, Maps.Length);
        SoundManager.PlayAcceptSound();
        SceneManager.LoadScene(Maps[Num]);
    }
    public void GoToMenu()
    {
        SoundManager.PlayAcceptSound();
        SceneManager.LoadScene("MainMenu");
    }
    public void Hire()
    {
        if(Lives<5 && Money >= LivesBuyRate)
        {
            Lives++;
            Money -= LivesBuyRate;
            SoundManager.PlayAcceptSound();
            PlayerPrefs.SetInt("Lives", Lives);
            PlayerPrefs.SetInt("Money", Money);
            PlayerPrefs.Save();

        }
        else
        {
            SoundManager.PlayDenySound();
        }
    }
}
