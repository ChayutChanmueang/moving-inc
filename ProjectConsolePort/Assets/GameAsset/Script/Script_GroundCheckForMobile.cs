using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_GroundCheckForMobile : MonoBehaviour
{
    public MobileMove MB;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Floor" || collision.tag=="GrabableObject")
        {
            if (MB.IsJumped == true)
            {
                MB.IsJumped = false;
            }
        }
    }
}
