using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_TimeTrap : MonoBehaviour
{
    float BeginTime = 0;
    float CurTime = 0;
    public float Cooldown = 5;
    public GrabableObject GO = null;
    public Animator animator = null;
    public enum State{
        Up,
        Down,
        Killing
    }
    public State TrapState = State.Down;
    // Start is called before the first frame update
    void Start()
    {
        BeginTime = Time.time;   
    }

    // Update is called once per frame
    void Update()
    {
        ScriptHandler();
        AnimationHandler();
    }
    public void ScriptHandler()
    {
        CheckState();
    }
    public void AnimationHandler()
    {
        if(TrapState == State.Up)
        {
            animator.SetBool("IsUp",true);
        }
        else
        {
            animator.SetBool("IsUp", false);
        }
        if (TrapState == State.Down)
        {
            animator.SetBool("IsDown", true);
        }
        else
        {
            animator.SetBool("IsDown", false);
        }
        if (TrapState == State.Killing)
        {
            animator.SetBool("IsKillPlayer", true);
        }
        else
        {
            animator.SetBool("IsKillPlayer", false);
        }
    }
    public void CheckState()
    {
        CurTime = Time.time;
        if (CurTime - BeginTime >= Cooldown)
        {
            if (TrapState == State.Down)
            {
                TrapState = State.Up;
                GO.Grabable = false;
            }
            else if (TrapState == State.Up)
            {
                TrapState = State.Down;
                GO.Grabable = true;
            }
            BeginTime = Time.time;
            Debug.Log(TrapState);
        }
    }
}
