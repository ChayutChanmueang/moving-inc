using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_PlayerInfoAbilityHandler : MonoBehaviour
{
    public float BasePlayerWeight = 60;
    public float PlayerWeight = 0;
    public GameObject GrabbingObj = null;
    public GameObject CanGrabItem = null;
    public GameObject Grabber = null;
    public bool AbleToDropItem = false;
    public int DropCooldown = 10;
    public int CurDropCooldown = 0;
    public PlayerController PC = null;
    // Start is called before the first frame update
    void Start()
    {
        PlayerWeight = BasePlayerWeight;
    }

    // Update is called once per frame
    void Update()
    {
        if(AbleToDropItem==false)
        {
            if(CurDropCooldown>=DropCooldown)
            {
                AbleToDropItem = true;
                CurDropCooldown = 0;
            }
            else
            {
                CurDropCooldown++;
            }
        }
        Vector3 LScale = Grabber.transform.localScale;
        if (PC.facing==PlayerController.PlayerDirection.Left)
        {
            if (Grabber.transform.localScale.x != -1)
            {
                LScale.x *= -1;
                Grabber.transform.localScale = LScale;
            }
        }
        else
        {
            if (Grabber.transform.localScale.x != 1)
            {
                LScale.x *= -1;
                Grabber.transform.localScale = LScale;
            }
        }
    }
    public void SetCanGrabObj(GameObject Obj)
    {
        if(Obj.gameObject.tag=="GrabableObject"&&CanGrabItem==null&& Obj.gameObject.GetComponent<GrabableObject>().Grabable == true)
        {
            CanGrabItem = Obj;
        }
    }
    public void UnSetCanGrabObj()
    {
        CanGrabItem = null;
    }
    public void GrabItem()
    {
        if(GrabbingObj==null&&CanGrabItem!=null)
        {
            GrabbingObj = CanGrabItem;
            //GrabbingObj.GetComponent<Collider>().enabled = false;
            CanGrabItem = null;
            GrabbingObj.GetComponent<GrabableObject>().IsGrab = true;
            AbleToDropItem = false;
            PlayerWeight += GrabbingObj.GetComponent<GrabableObject>().Weight;
        }
    }
    public void DropItem()
    {
        if (GrabbingObj !=null&&AbleToDropItem==true)
        {
            //GrabbingObj.GetComponent<Collider>().enabled = true;
            GrabbingObj.GetComponent<GrabableObject>().IsGrab = false;
            PlayerWeight -= GrabbingObj.GetComponent<GrabableObject>().Weight;
            GrabbingObj = null;
            AbleToDropItem = false;
        }
    }
}
