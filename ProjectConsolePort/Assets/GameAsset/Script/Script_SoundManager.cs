using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_SoundManager : MonoBehaviour
{
    public GameObject AcceptSound;
    public GameObject DenySound;
    public GameObject DeliverItemSound;
    public void PlayAcceptSound()
    {
        Instantiate(AcceptSound);
    }
    public void PlayDenySound()
    {
        Instantiate(DenySound);
    }

    public void PlayDeliverItemSound()
    {
        Instantiate(DeliverItemSound);
    }
}
