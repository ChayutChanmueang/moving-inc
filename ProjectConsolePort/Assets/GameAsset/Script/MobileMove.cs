using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileMove : MonoBehaviour
{
    private Rigidbody2D rb;
    private bool moveLeft;
    private bool moveRight;
    public float maxSpeed;
    bool AddSpeedLeft = false;
    bool AddSpeedRight = false;
    public bool IsJumped = false;
    public float speed = 5;
    public GameObject Player;
    public enum PlayerState
    {
        Idle,Walking,Jump
    }
    PlayerState PS;
    public Animator Animator;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        moveLeft = false;
        moveRight = false;
    }

    public void PointerDownLeft()
    {
        moveLeft = true;
        AddSpeedLeft = false;
    }

    public void PointerUpLeft()
    {
        moveLeft = false;
        if(!AddSpeedLeft)
        {
            rb.AddForce(Vector2.left * new Vector2(speed, 0.0f));
            AddSpeedLeft = true;
        }
    }

    public void PointerDownRight()
    {
        moveRight = true;
        AddSpeedRight = false;
    }

    public void PointerUpRight()
    {
        moveRight = false;
        if(!AddSpeedRight)
        {
            rb.AddForce(Vector2.right * new Vector2(speed, 0.0f));
            AddSpeedRight = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        HandleAnimation();
    }

    private void MovePlayer()
    {
        if (moveLeft)
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            PS = PlayerState.Walking;
            if(Player.transform.localScale.x > 0)
            {
                Player.transform.localScale = new Vector3(-1, Player.transform.localScale.y, Player.transform.localScale.z);
            }
            Debug.Log("left");
        }
        else if (moveRight)
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
            PS = PlayerState.Walking;
            if (Player.transform.localScale.x < 0)
            {
                Player.transform.localScale = new Vector3(1, Player.transform.localScale.y, Player.transform.localScale.z);
            }
            Debug.Log("right");
        }
        else if(!moveRight && !moveLeft && !IsJumped)
        {
            PS = PlayerState.Idle;
        }
        Vector2 horizonalVel = new Vector2(rb.velocity.x, rb.velocity.y);
        horizonalVel = Vector2.ClampMagnitude(horizonalVel, maxSpeed);
        rb.velocity = new Vector2(horizonalVel.x, rb.velocity.y);
    }
    public void Jump()
    {
        if(!IsJumped)
        {
            rb.AddForce(Vector2.up * new Vector2(rb.velocity.x, 24.0f), ForceMode2D.Impulse);
            PS = PlayerState.Jump;
            IsJumped = true;
        }
    }
    private void HandleAnimation()
    {
        if(PS == PlayerState.Idle)
        {
            Animator.SetBool("isIdle", true);
        }
        else
        {
            Animator.SetBool("isIdle", false);
        }
        if(PS == PlayerState.Walking)
        {
            Animator.SetBool("isWalking", true);
        }
        else
        {
            Animator.SetBool("isWalking", false);
        }
        if(PS==PlayerState.Jump)
        {
            Animator.SetBool("isJumping", true);
        }
        else
        {
            Animator.SetBool("isJumping", false);
        }

    }

    private void FixedUpdate()
    {
        //rb.velocity = new Vector2(horizontalMove, rb.velocity.y);
        Vector2 horizonalVel = new Vector2(rb.velocity.x, rb.velocity.y);
        horizonalVel = Vector2.ClampMagnitude(horizonalVel, maxSpeed);
        rb.velocity = new Vector2(horizonalVel.x,rb.velocity.y);
    }
}
