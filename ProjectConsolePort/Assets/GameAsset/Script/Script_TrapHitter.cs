using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_TrapHitter : MonoBehaviour
{
    public Script_TimeTrap Trap = null;
    public SpriteRenderer PlayerSprite = null;
    float BeginDeadTime = 0;
    float SpawnDelay = 0;
    GameObject PlayerRef = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Trap.TrapState==Script_TimeTrap.State.Killing && Time.time - BeginDeadTime >= SpawnDelay)
        {
            Trap.TrapState = Script_TimeTrap.State.Down;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Enter Trap !");
        if (Trap.TrapState == Script_TimeTrap.State.Up)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                collision.gameObject.GetComponent<Health>().TakeDamage(collision.gameObject.GetComponent<Health>().currentHealth);
                Trap.TrapState = Script_TimeTrap.State.Killing;
                PlayerSprite.enabled = false;
                BeginDeadTime = Time.time;
                SpawnDelay = collision.gameObject.GetComponent<Health>().respawnWaitTime;
                collision.gameObject.GetComponent<Script_PlayerInfoAbilityHandler>().DropItem();
                PlayerRef = collision.gameObject;
                PlayerRef.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            }
        }
    }
}
