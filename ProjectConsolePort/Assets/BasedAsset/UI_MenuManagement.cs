using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MenuManagement : MonoBehaviour
{
    public GameObject m_StartPanel; 
    public GameObject m_OptionPanel; 
    public void OnBackEvent() 
    {
        m_StartPanel.SetActive(true); 
        m_OptionPanel.SetActive(false); 
    }
    public void OnStartEvent()
    {
        m_StartPanel.SetActive(false);
        m_OptionPanel.SetActive(false);
    }
    public void OnOptionEvent() { 
        m_StartPanel.SetActive(false);
        m_OptionPanel.SetActive(true); 
    }
    public void OnExitEvent() { Application.Quit(); }
    public void OnSlideValueUpdateEvent(Slider _slider) 
    { 
        Debug.Log("Value : " + _slider.value); 
    }
    public void OnToggleUpdateEvent(Toggle _tog) 
    { 
        Debug.Log("Value : " + _tog.isOn); 
    }
}
